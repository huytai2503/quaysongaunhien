﻿package edu.khtn.quaysongaunhien;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
//	Test
public class MainActivity extends AppCompatActivity {
    TextView[] arrLinkSo = new TextView[10];
    TextView[] arrLinkTen = new TextView[10];
    ArrayList<String> arrHocVien = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void setRanNum(View v){
        taoArrLinkSo();
        taoArrLinkTen();
        arrHocVien.clear();
        for (int i = 0; i < arrLinkTen.length; i++) {
            if (String.valueOf(arrLinkTen[i].getText()).isEmpty() == false)
                arrHocVien.add(String.valueOf(arrLinkTen[i].getText()));
        }
        if(arrHocVien.isEmpty()==false) {
            for (int i = 0; i < arrLinkTen.length; i++) {
                if(String.valueOf(arrLinkTen[i].getText()).isEmpty()){
                    for (int j = i+1; j < arrLinkTen.length; j++) {
                        if (String.valueOf(arrLinkTen[j].getText()).isEmpty()==false){
                            arrLinkTen[i].setText(arrLinkTen[j].getText());
                            arrLinkTen[j].setText(null);
                            break;
                        }
                    }
                }
            }
            for (int i = 0; i < arrLinkSo.length; i++) {
                arrLinkSo[i].setText(null);
            }
            Random rdm = new Random();
            String[] arrRdm = new String[arrHocVien.size()];
            String rdmNew = rdm.nextInt(arrHocVien.size()) + 1 + "";
            arrRdm[0] = rdmNew;
            arrLinkSo[0].setText(rdmNew);
            for (int i = 1; i < arrHocVien.size(); i++) {
                rdmNew = rdm.nextInt(arrHocVien.size()) + 1 + "";
                for (int j = 0; j < i; j++) {
                    if (rdmNew.contentEquals(arrRdm[j])) {
                        rdmNew = rdm.nextInt(arrHocVien.size()) + 1 + "";
                        j = -1;
                    }
                }
                arrRdm[i] = rdmNew;
                arrLinkSo[i].setText(rdmNew);
            }
        }
        else arrLinkTen[0].setText("Danh sách trống");
    }

    public  void setArrHV(View v){
        taoArrLinkTen();
        taoArrHocVien();
        for(int i=0;i<arrHocVien.size();i++){
            arrLinkTen[i].setText(arrHocVien.get(i));
        }
    }

    public void taoArrLinkSo(){
        arrLinkSo[0] = (TextView) findViewById(R.id.so01);
        arrLinkSo[1] = (TextView) findViewById(R.id.so02);
        arrLinkSo[2] = (TextView) findViewById(R.id.so03);
        arrLinkSo[3] = (TextView) findViewById(R.id.so04);
        arrLinkSo[4] = (TextView) findViewById(R.id.so05);
        arrLinkSo[5] = (TextView) findViewById(R.id.so06);
        arrLinkSo[6] = (TextView) findViewById(R.id.so07);
        arrLinkSo[7] = (TextView) findViewById(R.id.so08);
        arrLinkSo[8] = (TextView) findViewById(R.id.so09);
        arrLinkSo[9] = (TextView) findViewById(R.id.so10);
    }

    public void taoArrLinkTen(){
        arrLinkTen[0] = (TextView) findViewById(R.id.ten01);
        arrLinkTen[1] = (TextView) findViewById(R.id.ten02);
        arrLinkTen[2] = (TextView) findViewById(R.id.ten03);
        arrLinkTen[3] = (TextView) findViewById(R.id.ten04);
        arrLinkTen[4] = (TextView) findViewById(R.id.ten05);
        arrLinkTen[5] = (TextView) findViewById(R.id.ten06);
        arrLinkTen[6] = (TextView) findViewById(R.id.ten07);
        arrLinkTen[7] = (TextView) findViewById(R.id.ten08);
        arrLinkTen[8] = (TextView) findViewById(R.id.ten09);
        arrLinkTen[9] = (TextView) findViewById(R.id.ten10);
    }

    public void taoArrHocVien(){
        arrHocVien.clear();
        arrHocVien.add("Huytai Tran");
        arrHocVien.add("Phong Trần");
        arrHocVien.add("Pham Son");
        arrHocVien.add("N Đ Nhật Quang");
        arrHocVien.add("Dang Phuoc Dat");
        arrHocVien.add("Văn Thanh");
        arrHocVien.add("Tuấn Phạm");
        arrHocVien.add("Quang Việt");
        arrHocVien.add("Anh Thái");
    }
}
